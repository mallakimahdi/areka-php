<?php

require '../vendor/autoload.php';
use Mailgun\Mailgun;

# Instantiate the client.
$mgClient = new Mailgun('key-67f196b0aff5beb32aec7146a58f8755');
$domain = "https://api.mailgun.net/v3/sandbox2353ad4e420f4e318783ecf4006d84aa.mailgun.org/messages";

# Make the call to the client.
$result = $mgClient->sendMessage($domain, array(
    'from'    => 'Excited User <excited@samples.mailgun.org>',
    'to'      => 'Baz <mlkbenjamin94@gmail.com>',
    'subject' => 'Hello',
    'text'    => 'Testing some Mailgun awesomness!'
));

echo $result;