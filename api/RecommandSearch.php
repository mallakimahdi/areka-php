<?php

set_error_handler(
    create_function(
        '$severity, $message, $file, $line',
        'throw new ErrorException($message, $severity, $severity, $file, $line);'
    )
);

function classify($terms) {
    
    $results = [];
    
    foreach ($terms as $term) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://localhost:8020/classify");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(["text" => $term]));

        $res = curl_exec($ch);
        
        if(!$res) {
            continue;
        }
        
        $res = json_decode($res);
        
        if($res == null) {
            continue;
        }
        
        $classes = [];
        
        foreach ($res as $r) {
            $classes[] = [
                "key" => $r->key,
                "prob" => $r->prob
            ];
        }
        
        $results[] = $classes;
    }
    
    return $results;
}

function getRelatedTerms($term) {
    
    $nodeItem = getGraphNode($term);

    if($nodeItem != null) {
        
        $termScore = [];
        
        foreach ($nodeItem->nodeRelations as $nodeRelation) {

            if($nodeRelation->weight < 5) {
                continue;
            }

            $score = $nodeRelation->weight / (log($nodeRelation->idf) + log($nodeItem->idf));            

            if(array_key_exists($nodeRelation->destinationName, $termScore)) {
                $termScore[$nodeRelation->destinationName] = $termScore[$nodeRelation->destinationName] + $score;
            }
            else {
                $termScore[$nodeRelation->destinationName] = $score;
            }
        }
        
        foreach ($termScore as $key => $value) {
            $terms[$key] = (int) $value;
        }

        return $terms;
    }
    
    return [];
}

function getGraphNode($term) {
    $manager = new MongoDB\Driver\Manager();
    $cursor = $manager->executeQuery("areka.graph", new MongoDB\Driver\Query(['name' => $term]));
    
    foreach($cursor as $c) {
        return $c;
    }
    
    return null;
}

function lcm($a, $b) {
    if ($a == 0 || $b == 0) {
        return 0;
    }
    return ($a * $b) / gcd($a, $b);
}

function gcd($a, $b) {
    if ($a < 1 || $b < 1) {
        die("a or b is less than 1");
    }
    $r = 0;
    do {
        $r = $a % $b;
        $a = $b;
        $b = $r;
    } while ($b != 0);
    return $a;
}
/*
$classifyResult = classify(["خیاطی", "خیط"]);
echo json_encode($classifyResult);
 * 
 */