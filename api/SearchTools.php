<?php

include_once __DIR__ . '/DateUtil.php';

function mapSearchResultToJob($response) {
    
    $hitResult['totalCount'] = 0;
    $hitResult['items'] = array();
    
    include_once __DIR__ . '/LocationUtil.php';
    
    if($response != NULL) {
        $hits = $response["hits"];
        $totalCount = $hits["total"];
        
        $hitResult['totalCount'] = $totalCount;
        
        $locs = [];
        
        if(array_key_exists("aggregations", $response)) {
            
            $filterLocations = $response["aggregations"]["location_bulks"]["buckets"];

            $totalBulketCount = 0;

            for ($i = 0; $i<sizeof($filterLocations); $i++) {

                $l = new stdClass;

                $l->count = $filterLocations[$i]["doc_count"];
                
                $key = $filterLocations[$i]["key"];
                $location = LocationUtil::getLocationById($key);
                
                if($location == null || (array_key_exists('admin_level', $location) && $location['admin_level'] != 6)) {
                    continue;
                }
                
                $l->id = $location['id'];
                $l->locationName = $location['name'];
                
                if($l->locationName == null || empty($l->locationName)) {
                    $l->locationName = "untitled";
                }

                $locs[] = $l;

                $totalBulketCount += $l->count;
            }
            
            $hitResult['filterJobTypes'] = $response["aggregations"]["job_type_bulks"];
            
            if(array_key_exists("related_class_bulks", $response["aggregations"])) {
                $hitResult['relatedClassBulks'] = $response["aggregations"]["related_class_bulks"];
            }
        }
        
        $hitResult['filterLocations'] = $locs;
        
        foreach($hits["hits"] as $hit) {
            $item = $hit["_source"];
        }
        
        foreach ($hits["hits"] as $hit) {
            
            $item = $hit["_source"];
            
            $item["openInBrowser"] = false;
            
            if(array_key_exists("locationId", $item)) {
                $locIds = $item["locationId"];
                for($j=0; $j<sizeof($locIds); $j++) {
                    
                    $locIdKey = $locIds[$j]."";
                    
                    $location = LocationUtil::getLocationById($locIdKey);
                    
                    if($location == null) {
                        continue;
                    }
                    
                    $item['locationNames'][] = [
                        'cityName' => $location['name']
                    ];
                    
                    if(array_key_exists("centerGeoPoint", $location)
                            && $location['centerGeoPoint'] != null) {
                        $item['geoLocations'][] = $location['centerGeoPoint'];
                    }
                }
            }
            
            if(array_key_exists("companyId", $item) && !empty($item["companyId"])) {
                
                include_once __DIR__ . '/ElasticSearchHandler.php';
                
                $params = [
                    'index' => 'company',
                    'type' => 'company',
                    'id' => $item["companyId"]
                ];
                
                try {
                    $co = ElasticSearchHandler::getInstance()->getElasticClient()->get($params)['_source'];

                    $company = new stdClass();
                    $company->id = $item['companyId'];
                    
                    unset($item["companyId"]);

                    if(array_key_exists("name", $co) && array_key_exists("address", $co)) {
                        $company->name = $co['name'];
                        $item['company'] = $company;
                    }

                } catch (Exception $ex) {
                    //echo $ex->getMessage();
                }
            }
            
            $phoneNumbers = [];
            if (array_key_exists("phoneNumber", $item)) {
                foreach ($item["phoneNumber"] as $phoneNumber) {
                    
                    $p = $phoneNumber;
                    if(strlen($p) == 12) {
                        $p = "+" . $p;
                    }
                    
                    $phoneNumbers[]= $p;
                }
            }
            $item['phoneNumber'] = $phoneNumbers;
            
            if(array_key_exists('highlight', $hit) && array_key_exists('body', $hit['highlight'])) {
                $item['highlight'] = $hit['highlight']['body'];
            }
            
            unset($item['locationCode']);
            unset($item['isDirty']);
            unset($item['isDeleted']);
            unset($item['assignedJobTag']);
            
            foreach ($item as $key => $value) {
                if(strpos($key, 'orig') === 0 || strpos($key, 'assignedJobTag') === 0) {
                    unset($item[$key]);
                }
            }
            
            $hitResult['items'][] = $item;
        }
    }
    return $hitResult;
}

function saveSearch($deviceUuid, $queries, $locationIdList, $jobType) {
    $query = [
        'index' => 'search',
        'type' => 'search_query',
        'body' => [
            "query" => $queries,
            "locationId" => $locationIdList,
            "jobType" => $jobType,
            "deviceUuid" => $deviceUuid
        ]
    ];
    
    $res = ElasticSearchHandler::getInstance()->getElasticClient()->index($query);
    
    echo json_encode($res);
    
    return $res["id"];
}

function saveVisitJob($deviceUuid, $searchId, $jobId, $date, $resultIndex) {
    $query = [
        'index' => 'search',
        'type' => 'search_result',
        'body' => [
            "deviceUuid" => $deviceUuid,
            "searchQueryId" => $searchId,
            "jobId" => $jobId,
            "date" => $date,
            "resultIndex" => $resultIndex
        ]
    ];
    
    $res = ElasticSearchHandler::getInstance()->getElasticClient()->index($query);
}