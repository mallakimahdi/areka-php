<?php

class LocationUtil {
    
    var $locationMap = [
          9841 => ['id' => 2, 'name' => "آذربایجان شرقی"] , 9844 => ['id' => 3,'name' => "آذربایجان غربی"]
        , 9845 => ['id' => 4,'name' => "اردبیل"], 9831 => ['id' => 5,'name' => "اصفهان"]
        , 9826 => ['id' => 6,'name' => "البرز"], 9884 => ['id' => 7,'name' => "ایلام"]
        , 9877 => ['id' => 8,'name' => "بوشهر"], 9821 => ['id' => 9,'name' => "تهران"]
        , 9838 => ['id' => 10,'name' => "چهار محال و بختیاری"], 9856 => ['id' => 11,'name' => "خراسان جنوبی"]
        , 9851 => ['id' => 12,'name' => "خراسان رضوی"], 9858 => ['id' => 13,'name' => "خراسان شمالی"]
        , 9861 => ['id' => 14,'name' => "خوزستان"], 9824 => ['id' => 15,'name' => "زنجان"]
        , 9823 => ['id' => 16,'name' => "سمنان"], 9854 => ['id' => 17,'name' => "سیستان و بلوچستان"]
        , 9871 => ['id' => 18,'name' => "فارس"], 9828 => ['id' => 19,'name' => "قزوین"]
        , 9825 => ['id' => 20,'name' => "قم"], 9817 => ['id' => 25,'name' => "گلستان"]
        , 9813 => ['id' => 26,'name' => "گیلان"], 9866 => ['id' => 27,'name' => "لرستان"]
        , 9811 => ['id' => 28,'name' => "مازندران"], 9886 => ['id' => 29,'name' => "مرکزی"]
        , 9876 => ['id' => 30,'name' => "هرمزگان"], 9881 => ['id' => 31,'name' => "همدان"]
        , 9887 => ['id' => 21,'name' => "کردستان"], 9834 => ['id' => 22,'name' => "کرمان"]
        , 9883 => [ 'id' => 23, 'name' => "کرمانشاه" ], 9874 => ['id' => 24,'name' => "کهکیلویه و بویراحمد"]
        , 9835 => ['id' => 32,'name' => "یزد"]
    ];
    
    var $locationMapComplete = null;
    
    public function __construct() {
        $this->locationMapComplete = $this->locationMap;
    }
    
    static $cacheLockKey = [];
    
    static function getLocationById($locationId) {
        
        $cache = LocationUtil::$cacheLockKey;
        
        if(!array_key_exists($locationId, $cache)) {
            
            $params = [
                "index" => "location",
                "type" => "location",
                "id" => $locationId,
                "_source_include" => [ "id", "name", "admin_level"]
            ];
            
            try {
                $loc = ElasticSearchHandler::getInstance()->getElasticClient()->get($params);
                $cache[$locationId] = $loc["_source"];
            }
            catch(Exception $e) {
                //not important
                return null;
            }
        }
        
        return $cache[$locationId];
    }
            
    function getLocationByLocationCode($locationCode, $isCompleteName) {
        
        $key = $locationCode;
        if($this->strEndsWith($key, "0000")) {
            $key = preg_replace('/0000/', '', $key);
        }
        else if($this->strEndsWith($key, "000")) {
            $key = preg_replace('/000/', '', $key);
        }
        else if($this->strEndsWith($key, "00")) {
            $key = preg_replace('/00/', '', $key);
        }
        
	$key = (int) $key;
        if(array_key_exists($key, $this->locationMap)) {
            return $this->locationMap[$key];
        }
        
        include_once __DIR__ . '/ElasticSearchHandler.php';
        
        $params = [
            "index" => "location",
            "type" => "location",
            "from" => 0,
            "size" => 1,
            "body" => [
                "query" => [
                    "term" => [
                        "locationCode" => (int) $key
                    ]
                ]
            ]
        ];
        
        $locs = ElasticSearchHandler::getInstance()->getElasticClient()->search($params)["hits"]["hits"];
        
        $name = null;
        $id = null;
        $centerGeoPoint = null;
        
        foreach($locs as $lo) {
            
            $loc = $lo['_source'];
            
            $name = $loc['name'];
            $id = $loc['id'];

            if($name != null && $name != "") {
                if($isCompleteName) {
                    $this->locationMapComplete[$key] = [
                        'id' => $id,
                        'name' => $name
                    ];
                }
                else {

                    $this->locationMap[$key] = [
                        'id' => $id,
                        'name' => $name
                    ];
                }
            }
            
            if(array_key_exists("centerGeoPoint", $loc)) {
                $centerGeoPoint = $loc->centerGeoPoint;
            }
            
            break;
        }
        
        if($isCompleteName && strlen($key) == 8) {
            $cityCode = substr($key, 0, 4);
            
            if (array_key_exists($cityCode, $this->locationMapComplete)) {
                
                $location = $this->locationMapComplete[$cityCode];
                
                if($name != null && $name != "") {
                    $name = $location['name'] . "، " . $name;
                }
                else {
                    $name = $location['name'];
                }
                
                $this->locationMapComplete[$key] = [
                    'id' => $id,
                    'name' => $name
                ];
            }
            
        }
        
        return [
            'id' => $id,
            'name' => $name,
            'centerGeoPoint' => $centerGeoPoint
        ];
    }
    
    static function getLcPropertiesByLocationIds($locationIdList) {
        $lc = 0;
        
        if ($locationIdList != null) {
            
            $locationCodeList = [];
            $elasticClient = Elasticsearch\ClientBuilder::create()->build();

            foreach ($locationIdList as $lil) {
                
                $params = [
                    'index' => 'location',
                    'type' => 'location',
                    'id' => $lil
                ];
                $loc = $elasticClient->get($params);
                
                $locationCodeList [] = $loc['_source']['locationCode'];
            }
            
	    foreach ($locationCodeList as $locationCode) {
                if($locationCode > $lc) {
                    $lc = $locationCode;
                }
            }
        }
        else {
            $lc = 98;
        }
        
        return LocationUtil::getLcPropertiesByLc($lc);
    }

    static function getLcPropertiesByLc($lc) {
        
        $lcMin = $lc;
        $lcMax = $lc;
        $lcInterval = 1;

        $lcSize = strlen($lc."");
        for ($i = 0; $i<(8-$lcSize) ; $i++) {
            $lcMin = $lcMin . "0";
            $lcMax = $lcMax . "9";
        }
        
        if($lcSize == 2) {
            $lcInterval = 10000;
        }
        else if($lcSize == 4) {
            $lcInterval = 1000;
        }
        else if ($lcSize == 5) {
            $lcInterval = 1;
        }
        
        $obj = new stdClass();
        $obj->lc = (int) $lc;
        $obj->lcMin = (int) $lcMin;
        $obj->lcMax = (int) $lcMax;
        $obj->lcInterval = (int) $lcInterval;
        
        return $obj;
    }
            
    function strEndsWith($haystack, $needle) {
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }
    
    static function getAllChildlLocationIds($locationIds) {
        
        $ids = array();
        
        $shapeFilters = [];
        $adminLevelFilters = [];
        
        foreach ($locationIds as $locationId) {
            
            $shapeFilters[] = [
                "geo_shape"=> [
                    "geometry"=> [
                        "indexed_shape"=> [
                            "index"=> "location",
                            "type"=> "location",
                            "id"=> $locationId,
                            "path"=> "geometry"
                        ],
                        "relation"=> "within"
                    ]
                ]
            ];
            
            $location = LocationUtil::getLocationById($locationId);
            if($location != null) {
                $adminLevel = $location["admin_level"];
                $adminLevelFilters[] = [
                    "term" => [
                        "admin_level" => $adminLevel + 2
                    ]
                ];
            }
            
            $ids[] = $locationId;
        }
        
        $body = [
            "_source" => false,
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "bool" => [
                                "should" => $shapeFilters
                            ]
                        ],
                        [
                            "bool" => [
                                "should" => $adminLevelFilters
                            ]
                        ]
                    ]
                ]
            ]
        ];
        
        $params = [
            "index" => "location",
            "type" => "location",
            "body" => $body,
            "size" => 1000
        ];
        
        //echo json_encode($body) . " *** ";
        
        $locs = ElasticSearchHandler::getInstance()->getElasticClient()
                ->search($params)["hits"]["hits"];
        
        foreach ($locs as $loc) {
            $ids[] = $loc["_id"];
        }
        
        return $ids;
    }
    
    static function getLocationByName($locationName) {
        $params = [
            "index" => "location",
            "type" => "location",
            "body" => [
                "_source" => false,
                "query" => [
                    "term" => [
                        "name.raw" => $locationName
                    ]
                ]
            ]
        ];
        
        $locs = ElasticSearchHandler::getInstance()->getElasticClient()
                ->search($params)["hits"]["hits"];
        
        $ids = array();
        
        foreach ($locs as $loc) {
            $ids[] = $loc["_id"];
        }
        
        return $ids;
    }
}