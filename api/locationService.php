<?php

if(isset($_GET["locationId"])) {
    $locationId = $_GET["locationId"];
    
    require '../vendor/autoload.php';
    include_once './ElasticSearchHandler.php';
    include_once './LocationUtil.php';
    
    $location = LocationUtil::getLocationById($locationId);
    echo json_encode($location);
}