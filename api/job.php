<?php

require '../vendor/autoload.php';
require_once 'ElasticSearchHandler.php';
require './config.php';
include_once './StringNormilizer.php';

if(isset($_GET["jobSuggest"])) {
    $q = $_GET["jobSuggest"];
    
    $suggests = ElasticSearchHandler::getInstance()->jobSuggest(normalize($q));

    print json_encode($suggests);
    die();
}
else if(isset ($_GET["areaNameSuggest"])) {
    $suggestionList = ElasticSearchHandler::getInstance()->areaNameSuggest($_GET["areaNameSuggest"]);
    print json_encode($suggestionList);
    die();
}
else if(isset($_GET["q"]) || isset($_GET["companyId"])) {
    
    include_once './SearchTools.php';
    
    $start_search_date = current_millis();
    
    $jobType = null;
    $queries = null;
    $lat = null;
    $lon = null;
    $dis = null;
    $locationId = null;
    $locationIdList = null;
    $lastSearchDate = null;
    $companyId = null;
    $page = 0;
    $sortType = "relevance";
    $jobIds = null;
    $pageSize = 20;
    $correctQuery = true;
    $exactSearch = false;
    $deviceUuid = null;
    
    if(isset($_GET["q"])) {
        $queries = explode(",", normalize($_GET["q"]));
    }
    if(isset($_GET["jobType"])) {
        $jobType = $_GET["jobType"];
    }
    if(isset($_GET["dis"])) {
        $dis = $_GET["dis"];
    }
    if(isset($_GET["lat"])) {
        $lat = $_GET["lat"];
        $lon = $_GET["lon"];
    }
    if(isset($_GET["locationId"])) {
        $locationIds = $_GET["locationId"];
        if($locationIds != 0) {
            include_once './SearchTools.php';
            
            $locationIdList = [];
            $locations = explode(",", $locationIds);
            
            foreach ($locations as $loc) {
                $locationIdList[] = $loc;
            }
            
            array_unique($locationIdList);
        }
    }
    if(isset($_GET["locationName"])) {
        include_once './LocationUtil.php';
        $locationIdList = LocationUtil::getLocationByName($_GET["locationName"]);
    }
    if(isset($_GET["lastSearchDate"])) {
        $lastSearchDate = $_GET["lastSearchDate"];
    }
    if(isset($_GET["companyId"])) {
        $companyId = $_GET["companyId"];
    }
    
    if(isset($_GET["page"])) {
        $page = $_GET["page"];
    }
    if(isset($_GET["pageSize"])) {
        $ps = $_GET["pageSize"];
        if($ps < 50) {
            $pageSize = $ps;
        }
    }
    if(isset($_GET["correctQuery"])) {
        $correctQuery = $_GET["correctQuery"];
    }
    if(isset($_GET["sortType"])) {
        $sortType = $_GET["sortType"];
    }
    if(isset($_GET["jobIds"])) {
        $jobIds = explode(",", $_GET["jobIds"]);
    }
    if(isset($_GET["exactSearch"])) {
        $exactSearch = $_GET["exactSearch"];
    }
    if(isset($_GET["deviceUuid"])) {
        $deviceUuid = $_GET["deviceUuid"];
    }
    
    $correctQueryRes = null;
    
    if(sizeof($queries) == 1) {
        
        $res = ElasticSearchHandler::getInstance()->getSpellCorrect($queries[0]);
        if($res != null) {
            $correctQueryRes = $res["_source"]["to"];
            $queries[] = $correctQueryRes;
        }
        
        $synonyms = [];
        $synonymHits = ElasticSearchHandler::getInstance()->getSynonyms($queries[0]);
        foreach ($synonymHits as $hit) {
            foreach ($hit["_source"]["synonyms"] as $synonym) {
                $synonyms[] = $synonym;
            }
        }
        foreach ($synonyms as $synonym) {
            if(!in_array($synonym, $queries)) {
                $queries[] = $synonym;
            }
        }
    }
    
    $restrictedClasses = null;
    $classifyTookTime = 0;
    
    if($queries != null && !$exactSearch) {
        
        $restrictedClasses = [];
        
        include_once './RecommandSearch.php';
        
        $start_classify_time = current_millis();
        $classifyResults = classify($queries);
        $classifyTookTime = current_millis() - $start_classify_time;

        foreach($classifyResults as $classifyResult) {
            foreach($classifyResult as $cr) {
                $restrictedClasses[] = $cr;
            }
        }
    }
    
    /*
    $correctQueryRes = null;
    
    if(sizeof($queries) == 1 && $correctQuery) {
        $correctQueryRes = ElasticSearchHandler::getInstance()->spellCorrect($queries[0]);
    }
     * 
     */
    
    $searchId = null;
    
    if($page == 0 && $deviceUuid != null && $companyId == null) {
        include_once "./UserDao.php";
        $searchId = saveSearchHistory($queries, $locationIdList, $jobType, $deviceUuid);
    }
    
    $response = ElasticSearchHandler::getInstance()->search($queries
        , $jobType, $dis, $lat, $lon, $locationIdList, $companyId, $page
        , $pageSize, $sortType, $lastSearchDate, $jobIds, $restrictedClasses);
    
    $searchResult = mapSearchResultToJob($response);
    
    /*
    $items = $searchResult["items"];
    for ($i=0; $i< sizeof($items); $i++) {
        $item = $items[$i];
        $searchResult["items"][$i]["url"] = $_SERVER['SERVER_NAME'] . "/api/url.php?url=" . 
            $_SERVER['SERVER_NAME'] . "/#/PosterDetailPage/" . $item["jobId"]
            . "&jobId=" . $item["jobId"] ."&deviceUuid=". $deviceUuid . "&resultIndex="
            . $i ."&searchId=". $searchId;
    }
     * 
     */
    
    $searchResult["correctQuery"] = $correctQueryRes;
    $searchResult["searchId"] = $searchId;
    
    $searchResult["took"] = current_millis() - $start_search_date;
    $searchResult["classifyTookTime"] = $classifyTookTime;
    
    print json_encode($searchResult);
    die();
}
else if(isset($_GET["jobIds"])) {
    $content = file_get_contents("php://input");
    $jobIds = json_decode($content, TRUE);
    
    $response = ElasticSearchHandler::getInstance()->getJobsByIds($jobIds);
    
    include_once './SearchTools.php';
    print json_encode(mapSearchResultToJob($response));
    die();
}
else if(isset ($_GET["similarityDoc"])) {
    $jobId = $_GET["similarityDoc"];
    
    $response = ElasticSearchHandler::getInstance()->getSimilarityJob($jobId);
    
    include_once './SearchTools.php';
    $res = mapSearchResultToJob($response);
    print json_encode($res);
    die();
}
else if(isset ($_GET["search_history"])) {
    
    $requestMethod = $_SERVER['REQUEST_METHOD'];
    if($requestMethod == 'POST') {
        include_once './RecommandSearch.php';
    
        $content = file_get_contents("php://input");
        $searchHistories = json_decode($content, TRUE);
        $results = array();

        $counter = 1;
        foreach ($searchHistories as $item) {

            $locationIdList = [];
            if(array_key_exists("locationIds", $item) && !empty($item["locationIds"])
                    && $item["locationIds"] != null && $item["locationIds"] != "null") {
                foreach (explode(",", $item["locationIds"]) as $locId) {
                    $locationIdList[] = $locId;
                }
            }

            $query = $item["query"];

            $restrictedClasses = null;

            $classifyResults = classify([$item["query"]]);
            if(!empty($classifyResults)) {
                $restrictedClasses = $classifyResults[0];
            }

            $res = ElasticSearchHandler::getInstance()->jobCount($query, $locationIdList
                , $item["lastUpdateDate"], $restrictedClasses);
            $results[] = $res["count"];

            $counter++;
        }

        print(json_encode($results));
    }
    else if($requestMethod == 'DELETE') {
        include_once './UserDao.php';
        
        try {
            $user = getUserByDeviceUuid($_GET["deviceUuid"]);    
            deleteSearchHistory($user);
        }
        catch(Exception $e) {
            //
        }
    }
    
    die();
}
else if(isset ($_GET["countAllNewJobsLastWeek"])) {
    $result = ElasticSearchHandler::getInstance()->countAllNewJobsLastWeek();
    print(json_encode(array("count" => $result["count"])));
    die();
}
else if(isset($_GET["whereAmI"])) {
    
    if(isset($_GET["lat"]) && isset($_GET["lon"])) {
        $lat = $_GET['lat'];
        $lon = $_GET['lon'];
        
        include_once './ElasticSearchHandler.php';
        
        $params = [
            'index' => 'location',
            'type' => 'location',
            'body' => [
                '_source' => ["name", "id"],
                'query' => [
                    'bool'=> [
                        'filter' =>[
                            [
                                'geo_shape' => [
                                    'geometry' => [
                                        'shape'=> [
                                            'type' => 'point',
                                            'coordinates' => [
                                                (double) $lon, 
                                                (double) $lat
                                            ]
                                        ],
                                        'relation' => 'contains'
                                    ]
                                ]
                            ],
                            [
                                'term' => [
                                    'properties.admin_level' => "4"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        
        //echo json_encode($params);
        
        $locs = ElasticSearchHandler::getInstance()->getElasticClient()
                ->search($params)["hits"]["hits"];

        $id = null;
        foreach ($locs as $l) {
            
            $loc = $l["_source"];
            
            $id = $loc['id'];
            $name = $loc['name'];
            break;
        }
        
        $result = null;
        if($id != null) {
            $result = [
                "id" => $id, 
                "city"=> $name
            ];
        }
        
        print(json_encode($result));
    }
    else {
        include './geoip.php';
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        print(json_encode(getCityOfIp($ipAddress)));
    }
    die();
}
else if(isset ($_GET["popularJobs"])) {

    $res = ElasticSearchHandler::getInstance()->getPopularJobs();
    $buckets = $res["aggregations"]["popular_jobs"]["buckets"];
    
    print(json_encode($buckets));
    
    die();
}
else if(isset ($_GET["favoriteJobs"])) {
    $content = file_get_contents("php://input");
    $favoriteJobs = json_decode($content, TRUE);
    
    if(sizeof($favoriteJobs) > 50) {
        die();
    }
    
    $response = ElasticSearchHandler::getInstance()->getJobListByIds($favoriteJobs);
    
    include_once './SearchTools.php';
    echo json_encode(mapSearchResultToJob($response));
    die();
}
