#!/usr/bin/php
<?php

include __DIR__ . '/../../vendor/autoload.php';
include __DIR__ . '/../ElasticSearchHandler.php';
include __DIR__ . '/./TelegramCommons.php';
include __DIR__ . '/../SearchTools.php';
include __DIR__ . '/../LocationUtil.php';
include __DIR__ . '/../RecommandSearch.php';
include __DIR__ . '/../UserDao.php';

Logger::configure(__DIR__ . "/../../log4php_config.xml");

$IS_SENDING_TO_CHANNEL_ENABLED = false;
$IS_SENDING_TO_BOT_ENABLED = true;
$SEARCH_RESULT_SIZE = 10;
$INDEX_NAME = 'telegram_subscribtion';

if($IS_SENDING_TO_BOT_ENABLED) {
    sendJobsToBot();
}

if($IS_SENDING_TO_CHANNEL_ENABLED) {
    sendJobsToChannel($jobIds);
}

function sendJobsToBot() {
    
    global $INDEX_NAME;
    
    $log = Logger::getLogger(basename(__FILE__));
    $log->info("start sending message to bot users");
    
    $elasticClient = ElasticSearchHandler::getInstance()->getElasticClient();
    
    $page = 0;
    
    while(true) {
        $params = [
            'index' => $INDEX_NAME,
            'type' => 'telegram_subscribtion',
            'from' => $page * 100,
            'size' => 100,
            'body' => [
                'query' => [
                    'bool' => [
                        'filter' => [
                            [
                                'term' => [
                                    'active' => true
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        
        $users = $elasticClient->search($params)['hits']['hits'];
        
        if(sizeof($users) === 0) {
            break;
        }
        
        sendJobForUser($users);
        
        $page++;
    }
}

function sendJobForUser($users) {
    
    global $SEARCH_RESULT_SIZE;
    
    $log = Logger::getLogger(basename(__FILE__));
    
    foreach ($users as $u) {
        
        $user = $u['_source'];
        
        if(!array_key_exists("searchId", $user)) {
            continue;
        }

        if(array_key_exists("active", $user) && !$user['active']) {
            continue;
        }
        
        $telegramStartSendingJobDate = current_millis();
        
        $chatId = $u['_id'];
        
        $searchQueries = [];
        $locationIds = [];

        $restrictedClasses = [];

        foreach ($user['searchId'] as $searchId) {
            
            $params = [
                "index" => "search_history",
                "type" => "search_history",
                "id" => $searchId
            ];
            
            $history = ElasticSearchHandler::getInstance()->getElasticClient()->get($params)["_source"];
            
            foreach ($history['query'] as $query) {
                $searchQueries[] = $query;

                $classifyResults = classify([$query]);

                if(!empty($classifyResults)) {

                    foreach($classifyResults[0] as $classifyResult) {
                        if($classifyResult["prob"] < 0.001) {
                            break;
                        }

                        $restrictedClasses[] = $classifyResult;
                    }
                }
            }

            if($history['locationId'] != null) {
                foreach ($history['locationId'] as $lId) {
                    $locationIds[] = $lId;
                }
            }
        }

        if(sizeof($searchQueries) == 0) {
            continue;
        }

        $telegramLastSentJobDate = current_millis() - (14 * 24 * 60 * 60 * 1000);
        if(array_key_exists("telegramLastSentJobDate", $user)) {
            $telegramLastSentJobDate = $user['telegramLastSentJobDate'];
        }

        if(array_key_exists("periodNotification", $user)) {
            if($telegramLastSentJobDate + $user["periodNotification"] > current_millis()) {
                continue;
            }
        }

        $locationIds = array_unique($locationIds);

        $results = ElasticSearchHandler::getInstance()->search($searchQueries
                , null, null, null, null, $locationIds, null, 0, $SEARCH_RESULT_SIZE
                , "relevance", $telegramLastSentJobDate, null, array_values($restrictedClasses));

        $items = mapSearchResultToJob($results)['items'];

        if(sizeof($items) == 0) {
            continue;
        }

        $counter = 0;

        foreach($items as $item) {
            try {
                sendLog($chatId, prepareJob($item));
                $counter ++;
                sleep(1);
            } 
            catch (Exception $ex) {

                if($ex->getCode() == 403) {
                    activeOrDeactiveUser($u["_id"], false);
                    continue 2;
                }
                else if($ex->getCode() == 429) {
                    sleep(100);
                }
            }
        }

        $log->info($counter . " job sent to chatId: ". $chatId);

        if($counter > 1) {

            $txt = "چند تا کار مرتبط با شما پیدا کردیم"
                 . "\n\n"
                 . "با استفاده از گزینه های زیر می توانید زمان ارسال آگهی ها را تغییر دهید. به صورت روزانه یا هفتگی یا اینکه ارسال نشود"
                 . "\n"
                 . "آگهی های جدید براساس مرتبط بودن با جستجوهای گذشته شما ارسال می شود. می توانید آنها را تغییر دهید";

            $msg = [
                'chat_id' => $chatId
                , "text" => $txt
                , 'reply_markup' => [
                    'inline_keyboard' => 
                    [
                        [
                            [
                                'text' => "لغو ارسال",
                                'callback_data' => '{"o":"cancelNotification"}'
                            ],
                            [
                                'text' => "هر هفته",
                                'callback_data' => '{"o":"weeklyNotification"}'
                            ],
                            [
                                'text' => "هر روز",
                                'callback_data' => '{"o":"dailyNotification"}'
                            ]
                        ],
                        [
                            [
                                'text' => "مشاهده جستجوهای اخیر",
                                'callback_data' => '{"o":"showSearchHistory"}'
                            ]
                        ]
                    ]
                ]
            ];

            try {    
                apiRequestJson("sendMessage", $msg);
            }
            catch (Exception $ex) {
                // not important
            }
        }

        $user["telegramLastSentJobDate"] = $telegramStartSendingJobDate;
        updateTelegramSubscribtion($chatId, $user);
    }
}

function array_contains_key($key, $array) {
    foreach ($array as $item) {
        if($item == $key) {
            return true;
        }
    }
    return false;
}

function sendJobsToChannel($jobIds) {
    
    $results = ElasticSearchHandler::getInstance()->search(null, null, null, null, null
        , null, null, 0, sizeof($jobIds) + 1, "date", null, $jobIds, null);

    $items = mapSearchResultToJob($results)['items'];
    $counter = 0;
    foreach($items as $item) {
        try {
            sendLog("@areka_job", prepareJob($item));
            sleep(1);
        }
        catch (Exception $ex) {
            if($ex->getCode() == 429) {
                sleep(50);
            }
        }

        $counter++;
        
        if($counter % 20 == 0) {
            sendLog("@areka_job", "با استفاده از ربات تلگرام و اپلیکیشن اندروید آرکا امکانات بیشتری برای جستجو دارید: "
                    . "\n" . " @areka_bot" ."\n". "لینک دانلود اپلیکیشن: ". "\n" . "http://areka.ir/areka.apk");
            $log->info("sent ". $counter ." job to channel");
        }
    }
}