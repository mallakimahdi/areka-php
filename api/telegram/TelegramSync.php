#!/usr/bin/php
<?php

include __DIR__ . '/../../vendor/autoload.php';
include_once __DIR__ . '/../ElasticSearchHandler.php';
include_once __DIR__ . '/./TelegramCommons.php';

Logger::configure(__DIR__ . "/../../log4php_config.xml");
$log = Logger::getLogger(basename(__FILE__));

$params = [
    'index' => 'telegram_delayed_message',
    'type' => 'telegram_delayed_message',
    'from' => 0,
    'size' => 100
];

$elasticClient = ElasticSearchHandler::getInstance()->getElasticClient();

$delayed_messages = $elasticClient->search($params)['hits']['hits'];

foreach ($delayed_messages as $m) {
    try {
	
        $message = $m['_source'];
        
	$msg = [];
        foreach ($message as $key => $value) {
            $msg[$key] = $value;
        }
	
        apiRequest("sendMessage", $msg);
        
        $params = [
            'index' => 'telegram_delayed_message',
            'type' => 'telegram_delayed_message',
            'id' => $m["_id"]
        ];
        
        $elasticClient->delete($params);
        
        $log->info("send delayed message");
    }
    catch (Exception $e) {
        $log->error("problem occured during send delayed message with error: ". $e->getMessage());
    }
}
