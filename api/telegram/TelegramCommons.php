<?php

define('BOT_TOKEN', '318513291:AAEavthZGpVXllK5bYfMGzDb2Fck8NXE9E0');
define('API_URL', 'https://api.telegram.org/bot'.BOT_TOKEN.'/');

include_once __DIR__ . '/../../vendor/autoload.php';
include __DIR__ . '/../PersianDate.php';
include_once __DIR__ . '/../ElasticSearchHandler.php';

function sendLog($chatId, $message) {
    apiRequest("sendMessage", [
                'chat_id' => $chatId,
                'text' => $message,
		'disable_web_page_preview' => true
            ]);
}

function sendTelegramRequestSafely($params) {
    
    try {
        apiRequest("sendMessage", $params);
    }
    catch (Exception $e) {
        if($e->getCode() == 429) {
            saveMessage($params);
        }
    }
}

function sendTelegramLogSafely($chatId, $message) {
    $msg = array(
                'chat_id' => $chatId,
                'text' => $message,
                'disable_web_page_preview' => true
            );
   
    try {
	apiRequest("sendMessage", $msg);
    }
    catch (Exception $e) {
        if($e->getCode() == 429) {
            saveMessage($msg);
        }
    }
}

function saveMessage($msg) {
    $params = [
        'index' => 'telegram_delayed_message',
        'type' => 'telegram_delayed_message',
        'body' => json_encode($msg)
    ];

    ElasticSearchHandler::getInstance()->getElasticClient()->index($params);
}

function apiRequestWebhook($method, $parameters) {
    if (!is_string($method)) {
        logError("Method name must be a string\n");
        return false;
    }

    if (!$parameters) {
        $parameters = array();
    } else if (!is_array($parameters)) {
        logError("Parameters must be an array\n");
        return false;
    }

    $parameters["method"] = $method;

    header("Content-Type: application/json");
    echo json_encode($parameters);
    return true;
}

function exec_curl_request($handle) {
    $response = curl_exec($handle);
    
    if ($response === false) {
        $errno = curl_errno($handle);
        $error = curl_error($handle);
        logError("Curl returned error $errno: $error\n");
        logError("Curl returned error $errno: $error\n");
        curl_close($handle);
        return false;
    }

    $http_code = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));
    curl_close($handle);

    if ($http_code >= 500) {
        // do not wat to DDOS server if something goes wrong
        sleep(10);
        return false;
    } else if ($http_code != 200) {
        $response = json_decode($response, true);
        logError("\nRequest has failed with error {$response['error_code']}: {$response['description']}\n");
        if ($http_code == 401) {
            throw new Exception('Invalid access token provided');
        }
        else if($http_code == 403) {
            throw new Exception("bot stoped by user", 403);
        }
        else if($http_code == 429) {
            throw new Exception("too many requests", 429);
        }
        return false;
    } else {
        $response = json_decode($response, true);
        if (isset($response['description'])) {
            logError("Request was successfull: {$response['description']}\n");
        }
        $response = $response['result'];
    }
    return $response;
}

function apiRequest($method, $parameters) {
    if (!is_string($method)) {
        logError("Method name must be a string\n");
        return false;
    }

    if (!$parameters) {
        $parameters = array();
    } else if (!is_array($parameters)) {
        logError("Parameters must be an array\n");
        return false;
    }

    foreach ($parameters as $key => &$val) {
        // encoding to JSON array parameters, for example reply_markup
        if (!is_numeric($val) && !is_string($val)) {
            $val = json_encode($val);
        }
    }
    $url = API_URL.$method.'?'.http_build_query($parameters);

    $handle = curl_init($url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($handle, CURLOPT_TIMEOUT, 60);

    return exec_curl_request($handle);
}

function apiRequestJson($method, $parameters) {
    if (!is_string($method)) {
        logError("Method name must be a string\n");
        return false;
    }

    if (!$parameters) {
        $parameters = array();
    } else if (!is_array($parameters)) {
        logError("Parameters must be an array\n");
        return false;
    }

    $parameters["method"] = $method;

    $handle = curl_init(API_URL);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($handle, CURLOPT_TIMEOUT, 60);
    curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parameters));
    curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

    return exec_curl_request($handle);
}

function logError($log) {
    echo $log;
    file_put_contents("./logs.txt", $log);
}

function startsWith($haystack, $needle) {
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function prepareJob($job) {
    
    $text = "";
    
    $text .= $job['title'] . "\n\n";
    
    if(array_key_exists('body', $job)) {
        $bodies = $job['body'];
        
        foreach ($bodies as $body) {
            $text .= $body . "\n";
        }

	if(mb_strlen($text) > 3000) {
                $text = mb_substr($text, 0, 3000) . "...";
         }
        
	$text .= "\n";
    }
    
    if(array_key_exists('phoneNumber', $job) && $job["phoneNumber"] != null && !empty($job["phoneNumber"])) {
        $phoneNumbers = $job['phoneNumber'];
        $text .= "شماره تماس: ";
        foreach ($phoneNumbers as $phoneNumber) {
            $text .= $phoneNumber ."\n";
        }
	$text .= "\n";
    }
    
    if(array_key_exists('email', $job) && $job["email"] != null && !empty($job["email"])) {
        $emails = $job['email'];
        $text .= "ایمیل: ";
        foreach ($emails as $email) {
            $text .= $email ."\n";
        }
	$text .= "\n";
    }
    
    if(array_key_exists('locationNames', $job) && $job["locationNames"] != null && !empty($job["locationNames"])) {
        $locationNames = $job['locationNames'];
	$text .= "شهر: ";
        foreach ($locationNames as $loc) {
            $text .= $loc['cityName'] . " ";
        }
        $text .= "\n";
    }
    
    if(array_key_exists("date", $job)) {
        $date = $job['date'];
        $text .= "\n" . "تاریخ آگهی: ". convertMillisToDate($date) . "\n";
    }
    
    if(array_key_exists("sourceCrawle", $job)) {
        $text .= "\n\n" . "لینک منبع: ". $job['sourceCrawle'] . "\n";
    }
    
    # $text .= "\n لینک:" . " http://areka.ir/#/PosterDetailPage/". $job["id"] . " \n";
    
    if(array_key_exists("jobTag", $job) && sizeOf($job["jobTag"] > 0)) {
        $text .= "\n" . "تگ ها: ";
        foreach ($job["jobTag"] as $tag) {
            $text .= "#". str_replace(' ', '_', $tag) ." ";
        }
        $text .= "\n";
    }
    
    return $text;
}