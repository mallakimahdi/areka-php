<?php

include_once __DIR__ . '/../../vendor/autoload.php';
include_once __DIR__ . '/TelegramCommons.php';
include_once __DIR__ . '/../UserDao.php';
include_once __DIR__ . '/../LocationUtil.php';

$content = file_get_contents("php://input");
$update = json_decode($content, true);

if (!$update) {
    // receive wrong update, must not happen
    exit;
}

if (isset($update["message"])) {
    processMessage($update["message"]);
} else if (isset($update["callback_query"])) {
    processCallBack($update["callback_query"]);
}

/**
 * @param $message
 */
function processMessage($message) {

    $chatId = $message['chat']['id'];
    $messageText = $message['text'];
    
    if (startsWith($messageText, "/start")) {
        $historyId = trim(str_replace("/start", "", $messageText));
        if($historyId == "") {
            sendTelegramLogSafely($chatId, "به بات آرکا خوش آمدید. برای استفاده از امکانات این بات ابتدا به سایت areka.ir مراجعه فرمایید");
            return;
        }
        
        $history = getSearchHistory($historyId);
        if($history == null) {
            sendTelegramLogSafely($chatId, "اطلاعات شما یافت نشد! لطفا مجددا اقدام فرمایید");
            return;
        }
        
        saveTelegramSubscribtion($chatId, $historyId);
        activeOrDeactiveTelegramUser($chatId, true);
        
        sendTelegramLogSafely($chatId, "ثبت نام شما با موفقیت انجام شد. آگهی های جدید در اولین فرصت برای شما ارسال خواهد شد");
    }
}

function processCallBack($callback) {
    $chatId = $callback["from"]["id"];
    $data = json_decode($callback["data"], true);
    
    //sendLog($chatId, json_encode($callback));
    
    if (!array_key_exists("o", $data)) { //operation
        $query = $data["q"];
        $cityId = $data["cId"];
        $page = $data["p"];
        
        if ($cityId == -1) {
	    $msg = [
                'chat_id' => $chatId
                , "text" => "کدام شهر؟"
                , 'reply_markup' => [
                    'inline_keyboard' => getCities($query, $page)
                ]
            ];

            sendTelegramRequestSafely($msg);
            return;
        }

        require '../../vendor/autoload.php';
        include '../ElasticSearchHandler.php';

        $locationIds = [$cityId];

        $response = ElasticSearchHandler::getInstance()->search($query, null, null, null, null
                , $locationIds, null, $page, 3, "relevance", null, null, null);

        include '../SearchTools.php';
        $items = mapSearchResultToJob($response)['items'];
	
        if(sizeof($items) == 0) {
            
            $txt = "";
            
            if($page == 0) {
                $txt = "متاسفانه موردی برای " . "'". $query ."' یافت نشد. " 
                        . "می تونید کلمه دیگه ای جستجو کنین";
                
            }
            else {
                $txt = "مورد دیگه ای یافت نشد. می تونین منتظر باشین تا هر موقع کار جدیدی اومد براتون بفرستیم";
            }
            
            sendTelegramLogSafely($chatId, $txt);
            
            return;
        }

        foreach ($items as $item) {
            sendTelegramLogSafely($chatId, prepareJob($item));
        }

        $msg = [
            'chat_id' => $chatId
            , "text" => "از بین گزینه های زیر یکی را انتخاب کنید"
            , 'reply_markup' => [
                'inline_keyboard' => [
                        [
                            [
                            'text' => "تغییر شهر",
                            'callback_data' => '{"o":"cC","q":"' . $query . '"}'
                        ],
                            [
                            'text' => "جستجوی جدید",
                            'callback_data' => '{"o":"nS","cId":' . $cityId . '}'
                        ]
                    ],
                        [
                            [
                            'text' => " →" . "صفحه بعد " . "(صفحه " . ($page + 1) . ") ",
                            'callback_data' => '{"q":"' . $query . '","cId":' . $cityId
                            . ',"p":' . ($page + 1) . '}'
                        ]
                    ]
                ]
            ]
        ];

        //sendLog($chatId, json_encode($callback));
        //apiRequestJson("sendMessage", $msg);
        sendTelegramRequestSafely($msg);

        if ($page == 0) {
            $manager = new MongoDB\Driver\Manager();
            $bulk = new MongoDB\Driver\BulkWrite;
            $bulk->update(["chatId" => $chatId]
                    , [
                '$set' => [
                    "chatId" => $chatId
                ],
                '$addToSet' => [
                    "queries" => [
                        "query" => $query,
                        "locationId" => $cityId
                    ]
                ]
                    ]
                    , ['upsert' => true]
            );

            $manager->executeBulkWrite('areka.telegram_searches', $bulk);
        }
    } 
    else {

        $operation = $data["o"];

        if ($operation == "nS") { // new search
            sendTelegramLogSafely($chatId, WELCOME_MESSAGE);
        } 
        else if ($operation == "cC") { //change city
            $msg = [
                'chat_id' => $chatId
                , "text" => "کدام شهر؟"
                , 'reply_markup' => [
                    'inline_keyboard' => getMostImportantCities($data["q"], 0)
                ]
            ];

            //apiRequestJson("sendMessage", $msg);
            sendTelegramRequestSafely($msg);
        }
        else if($operation == "cancelNotification") {
            
            foreach(getUsersByChatId($chatId) as $user) {
                activeOrDeactiveTelegramUser($user, false);
            }
            
            $cancelMsg = "اشتراک شما لغو شد. می تونید هر وقت خواستید دوباره فعالش کنید";
            
            $msg = [
                    'chat_id' => $chatId
                    , "text" => $cancelMsg
                    , 'reply_markup' => [
                        'inline_keyboard' => 
                        [
                            [
                                [
                                    'text' => "فعال سازی مجدد ارسال روزانه آگهی های جدید",
                                    'callback_data' => '{"o":"enableNotification"}'
                                ]
                            ]
                        ]
                    ]
                ];
                
            apiRequestJson("sendMessage", $msg);
        }
        else if($operation == "dailyNotification") {
            foreach (getUsersByChatId($chatId) as $user) {
                $user["periodNotification"] = 24 * 60 * 60 * 1000;
                updateUser($user);
            }
            
            sendLog($chatId, "آگهی های جدید هر روز برای شما ارسال خواهد شد");
        }
        else if($operation == "weeklyNotification") {
            foreach (getUsersByChatId($chatId) as $user) {
                $user["periodNotification"] = 7 * 24 * 60 * 60 * 1000;
                updateUser($user);
            }
            
            sendLog($chatId, "آگهی های جدید هفته ای یک روز برای شما ارسال خواهد شد");
        }
        else if($operation == "enableNotification") {
            foreach (getUsersByChatId($chatId) as $user) {
                activeOrDeactiveTelegramUser($user, true);
            }
            sendTelegramLogSafely($chatId, "با موفقیت فعال شد" 
                    . ". هر موقع آگهی جدید مرتبط با جستجوهای گذشتتون پیدا کنیم براتون می فرستیم");
        }
        else if ($operation == "clearSearchHistory") {
            
            foreach (getUsersByChatId($chatId) as $user) {
                clearUserHistory($user);
            }
            
            sendTelegramLogSafely($chatId, "سوابق جستجوهای شما پاک شد. می توانید دوباره از طریق سایت http://areka.ir شروع به جستجو کنید");
        }
        else if($operation == "showSearchHistory") {
            
            $txt = "لیست جستجوهای اخیر شما: \n\n";
            
            $counter = 1;
            
            foreach (getUsersByChatId($chatId) as $user) {
                foreach ($user["searchHistory"] as $history) {
                    
                    $txt .= $counter ."- ";
                    
                    foreach ($history["query"] as $query) {    
                        $txt .= $query . " ";
                    }
                    
                    if($history["locationId"] != null) {
                        
                        $txt .= " در ";
                        
                        foreach($history["locationId"] as $locationId) {
                            $txt .= " ". LocationUtil::getLocationById($locationId)["name"];
                        }
                    }
                    
                    $txt .= "\n";
                    $counter++;
                }
            }
            
            $msg = [
                'chat_id' => $chatId
                , "text" => $txt
                , 'reply_markup' => [
                    'inline_keyboard' => 
                    [
                        [
                            [
                                'text' => "حذف تمام جستجوهای اخیر",
                                'callback_data' => '{"o":"clearSearchHistory"}'
                            ]
                        ]
                    ]
                ]
            ];

            try {    
                apiRequestJson("sendMessage", $msg);
            }
            catch (Exception $ex) {
                // not important
            }
        }
    }
}

function getMostImportantCities($query, $page) {
    return [
            [
                [
                'text' => "اصفهان",
                "callback_data" => '{"q":"' . $query . '","cId":5,"p":' . $page . '}'
            ],
                [
                'text' => "البرز",
                'callback_data' => '{"q":"' . $query . '","cId":6,"p":' . $page . '}'
            ],
                [
                'text' => "تهران",
                'callback_data' => '{"q":"' . $query . '","cId":9,"p":' . $page . '}'
            ]
        ],
            [
                [
                    'text' => "خراسان رضوی",
                    'callback_data' => '{"q":"' . $query . '","cId":12,"p":' . $page . '}'
                ],
                [
                    'text' => "فارس",
                    "callback_data" => '{"q":"' . $query . '","cId":18,"p":' . $page . '}'
                ],
                [
                    'text' => "قم",
                    'callback_data' => '{"q":"' . $query . '","cId":20,"p":' . $page . '}'
                ]
        ],
            [
                [
                    'text' => 'نمایش تمامی شهرها',
                    'callback_data' => '{"q":"' . $query . '", "cId":-1,"p":' . $page . '}'
                ]
            ]
        ];
}

function getCities($query, $page) {
    return [
            [
                [
                'text' => "اصفهان",
                "callback_data" => '{"q":"' . $query . '","cId":5,"p":' . $page . '}'
            ],
                [
                'text' => "البرز",
                'callback_data' => '{"q":"' . $query . '","cId":6,"p":' . $page . '}'
            ],
                [
                'text' => "تهران",
                'callback_data' => '{"q":"' . $query . '","cId":9,"p":' . $page . '}'
            ]
        ],
            [
                [
                'text' => "خراسان رضوی",
                'callback_data' => '{"q":"' . $query . '","cId":12,"p":' . $page . '}'
            ],
                [
                'text' => "فارس",
                "callback_data" => '{"q":"' . $query . '","cId":18,"p":' . $page . '}'
            ],
                [
                'text' => "قم",
                'callback_data' => '{"q":"' . $query . '","cId":20,"p":' . $page . '}'
            ]
        ],
            [
                [
                'text' => "آذربایجان غربی",
                "callback_data" => '{"q":"' . $query . '","cId":3,"p":' . $page . '}'
            ],
                [
                'text' => "آذربایجان شرقی",
                "callback_data" => '{"q":"' . $query . '","cId":2,"p":' . $page . '}'
            ]
        ],
            [
                [
                'text' => "خوزستان",
                'callback_data' => '{"q":"' . $query . '","cId":14,"p":' . $page . '}'
            ],
                [
                'text' => "قزوین",
                "callback_data" => '{"q":"' . $query . '","cId":19,"p":' . $page . '}'
            ],
                [
                'text' => "سمنان",
                "callback_data" => '{"q":"' . $query . '","cId":16,"p":' . $page . '}'
            ]
        ],
            [
                [
                'text' => "زنجان",
                "callback_data" => '{"q":"' . $query . '","cId":15,"p":' . $page . '}'
            ],
                [
                'text' => "اردبیل",
                "callback_data" => '{"q":"' . $query . '","cId":4,"p":' . $page . '}'
            ],
                [
                'text' => "بوشهر",
                'callback_data' => '{"q":"' . $query . '","cId":8,"p":' . $page . '}'
            ]
        ],
            [
                [
                'text' => "سیستان و بلوچستان",
                'callback_data' => '{"q":"' . $query . '","cId":17,"p":' . $page . '}'
            ],
                [
                'text' => "چهارمحال و بختیاری",
                'callback_data' => '{"q":"' . $query . '","cId":10,"p":' . $page . '}'
            ]
        ],
            [
                [
                'text' => "خراسان جنوبی",
                'callback_data' => '{"q":"' . $query . '","cId":11,"p":' . $page . '}'
            ],
                [
                'text' => "خراسان شمالی",
                "callback_data" => '{"q":"' . $query . '","cId":13,"p":' . $page . '}'
            ]
        ],
            [
                [
                'text' => "ایلام",
                'callback_data' => '{"q":"' . $query . '","cId":7,"p":' . $page . '}'
            ],
                [
                'text' => "گلستان",
                'callback_data' => '{"q":"' . $query . '","cId":25,"p":' . $page . '}'
            ],
                [
                'text' => "گیلان",
                'callback_data' => '{"q":"' . $query . '","cId":26,"p":' . $page . '}'
            ]
        ],
            [
                [
                'text' => "لرستان",
                'callback_data' => '{"q":"' . $query . '","cId":27,"p":' . $page . '}'
            ],
                [
                'text' => "مرکزی",
                'callback_data' => '{"q":"' . $query . '","cId":29,"p":' . $page . '}'
            ],
                [
                'text' => "مازندران",
                'callback_data' => '{"q":"' . $query . '","cId":28,"p":' . $page . '}'
            ]
        ],
            [
                [
                'text' => "هرمزگان",
                'callback_data' => '{"q":"' . $query . '","cId":30,"p":' . $page . '}'
            ],
                [
                'text' => "همدان",
                'callback_data' => '{"q":"' . $query . '","cId":31,"p":' . $page . '}'
            ],
                [
                'text' => "کردستان",
                'callback_data' => '{"q":"' . $query . '","cId":21,"p":' . $page . '}'
            ]
        ],
            [
                [
                'text' => "کرمان",
                'callback_data' => '{"q":"' . $query . '","cId":22,"p":' . $page . '}'
            ],
                [
                'text' => "کرمانشاه",
                'callback_data' => '{"q":"' . $query . '","cId":23,"p":' . $page . '}'
            ],
                [
                'text' => "یزد",
                'callback_data' => '{"q":"' . $query . '","cId":32,"p":' . $page . '}'
            ]
        ]
    ];
}
