<?php

set_error_handler(
    create_function(
        '$severity, $message, $file, $line',
        'throw new ErrorException($message, $severity, $severity, $file, $line);'
    )
);

function getCityOfIp($ipAddress) {
    try {
        $res = file_get_contents("http://localhost:8030/geoip/" . $ipAddress);
        return json_decode($res);
    }
    catch (Exception $e) {
        return null;
    }
}