<?php

/**
 * Created by IntelliJ IDEA.
 * User: mahdi
 * Date: 3/19/16
 * Time: 6:30 PM
 */
class ElasticSearchHandler {
    
    public static function getInstance() {
        static $inst = null;
        if ($inst === null)
            $inst = new ElasticSearchHandler();

        return $inst;
    }

    public static $client;

    private function __construct() {
        ElasticSearchHandler::$client = Elasticsearch\ClientBuilder::create()
                ->setHosts(["https://search-myel-gmxvjw7w73r4h62pack6gbariu.us-east-2.es.amazonaws.com:443"])
                ->build();
    }
    
    public function getElasticClient() {
        return ElasticSearchHandler::$client;
    }

    public function jobSuggest($q) {
        
        $results = null;
        $fuzziness = 0;
        
        $body = [
            "query" => [
                "function_score" => [
                    "query" => [
                        "bool" => [
                            "must" => [
                                [
                                    $matchQuery = [
                                        "match_phrase_prefix" => [
                                            "name" => $q
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "field_value_factor" => [
                        "field" => "weight",
                        "modifier" => "log1p"
                    ]
                ]
            ]
        ];
        
        //echo json_encode($body) . " *** ";
        
        $params = [
            'index' => "tag",
            'body' => $body
        ];
        
        $hits = ElasticSearchHandler::$client->search($params)["hits"]["hits"];
        
        $results = [];
        
        foreach ($hits as $hit) {
            $results[] = [
                "text" => $hit["_source"]["name"]
            ];
        }
        
        if(empty($results)) {
            
            $params["body"]["query"]["function_score"]["query"]["bool"]["must"][0] = [
                "match" => [
                    "name" => [
                        "query" => $q,
                        "fuzziness" => "AUTO"
                    ]
                ]
            ];
            
            //echo json_encode($params) . " *** ";
            
            $hits = ElasticSearchHandler::$client->search($params)["hits"]["hits"];
            
            foreach ($hits as $hit) {
                $results[] = [
                    "text" => $hit["_source"]["name"]
                ];
            }
        }
        
        return $results;
    }
    
    private function suggestWithFuzziness($index, $q, $fuzziness) {
        $body['suggest'] = [
            'tag_suggests' => [
                "prefix" => $q,
                "completion" => [
                    "field" => "suggest",
                    "fuzzy" => [
                        "fuzziness" => $fuzziness
                    ],
                    "size" => 10
                ]
            ]
        ];
        
        $params = [
            'index' => $index,
            'body' => $body
        ];
        
        //echo json_encode($body). " *** ";
        
        $res = ElasticSearchHandler::$client->search($params);
        
        if(!array_key_exists('suggest', $res)) {
            return;
        }
        
        $options = $res['suggest']['tag_suggests'][0]['options'];
        
        $results = [];
        
        foreach ($options as $option) {
            if($option["_source"]["suggest"]["weight"] > 0) {
                $results[] = $option;
            }
            
        }
        
        return $results;
    }
    
    public function companySuggestion($q) {
        $query = [
            "suggest" => [
                "text" => $q,
                "completion" => [
                    "field" => "name",
                    "fuzzy" => [
                        "fuzziness" => 1
                    ]
                ]
            ]
        ];

        $params = [
            'index' => 'company',
            'body' => $query
        ];
        
        return ElasticSearchHandler::$client->suggest($params);
    }

    public function areaNameSuggest($q) {
        
        $body["query"] = [
            "function_score" => [
                "query" => [
                    "bool" => [
                        "must" => [
                            [
                                "match" => [
                                    "name" => [
                                        "query" => $q,
                                        "fuzziness" => "AUTO"
                                    ]
                                ]
                            ]
                        ],
                        'must_not' => [
                            'term' => [
                                "admin_level" => 2
                            ]
                        ]
                    ]
                ],
                "field_value_factor" => [
                    "field" => "weight",
                    "modifier" => "log1p"
                ]
            ]
        ];
        
        $params = [
            'index' => "location",
            'body' => $body,
            '_source_include' => ['name', 'id', 'complete_name']
        ];
        
        $res = ElasticSearchHandler::$client->search($params);
        
        $suggestionList = [];
        
        foreach ($res["hits"]["hits"] as $suggest) {
            
            $doc = $suggest["_source"];
            
            $item = array();
            $item["id"] = $doc['id'];
            
            if(array_key_exists('complete_name', $doc) && $doc['complete_name'] != null
                    && $doc['complete_name'] != "") {
                $item['city'] = $doc['complete_name'];
            }
            else {
                $item["city"] = $doc['name'];
            }

            $suggestionList[] = $item;
        }
        
        return $suggestionList;
    }

    public function jobCount($q, $locationIdList, $lastUpdate, $restrictedClasses) {
        
        if ($q != null) {
            $query['must'][] = [
                'multi_match' => [
                    'query' => $q,
                    'fields' => ['title', 'body', 'jobTag']
                ]
            ];
        }

        if ($locationIdList != null) {
            
            include_once __DIR__ . '/LocationUtil.php';
            
            if($locationIdList != null) {
            
                $allLocationIds = LocationUtil::getAllChildlLocationIds($locationIdList);

                $query['filter'][] = [ 'terms' => [
                        "locationId" => $allLocationIds
                    ]
                ];
            }
        }
        
        if ($lastUpdate != null) {
            
            $query['filter'][] = [ 'range' => [
                    'date' => ["gte" => $lastUpdate]
                ]
            ];
        }
        
        if($restrictedClasses != null && !empty($restrictedClasses)) {
            
            foreach ($restrictedClasses as $rc) {
                $query['filter'][] = [
                    "range" => [
                        "assignedJobTag_". $rc["key"] => [
                            "gte" => 0.01
                        ]
                    ]
                ];
            }
        }

        $body['query']['bool'] = $query;

        $elasticsearchQuery = [
            'index' => 'job',
            'type' => 'job',
            'body' => $body
        ];

        //print json_encode($body);

        return ElasticSearchHandler::$client->count($elasticsearchQuery);
    }
    
    public function getSpellCorrect($term) {
        $elasticsearchQuery = [
            'index' => 'keywords',
            'type' => 'query',
            'id' => $term
        ];
        
        try {
            return ElasticSearchHandler::$client->get($elasticsearchQuery);
        }
        catch(Exception $e) {
            //not important
        }
    }
    
    public function getSynonyms($term) {
        $elasticsearchQuery = [
            'index' => 'synonym',
            'type' => 'synonym',
            'body' => [
                "query" => [
                    "bool" => [
                        "should" => [
                            "term" => [
                                "synonyms" => $term
                            ]
                        ]
                    ]
                ]
            ]
        ];

        //print json_encode($body);

        return ElasticSearchHandler::$client->search($elasticsearchQuery)["hits"]["hits"];
    }

    public function search($queries, $jobType, $dis, $lat, $lon, $locationIdList, $companyId, $page
            , $pageSize, $sortType, $lastSearchDate, $jobIds, $restrictedClasses) {
        
        $query = [];

        if($jobIds != null) {
            
            $query['filter'][] = [ 'terms' => [
                    'id' => $jobIds
                ]
            ];
        }

        if ($jobType != null) {
            $query['filter'][] = [ 'terms' => [
                    'jobType' => explode(",", $jobType)
            ]];
        }

        if ($lat != null) {
            $query['filter'][] = ['geo_distance' => [
                    "distance" => $dis . "km",
                    "geoLocations" => [
                        "lat" => $lat,
                        "lon" => $lon
                    ]
            ]];
        }
        
        $maxProb = 0;
        
        if($restrictedClasses != null && !empty($restrictedClasses)) {
            
            $minProbClass = [];
            
            foreach ($restrictedClasses as $rc) {
                if($rc["prob"] > $maxProb) {
                    $maxProb = $rc["prob"];
                }
                
                $tag = str_replace(".", " dot ", $rc["key"]);
                $minProbClass[] = [
                    "range" => [
                        "assignedJobTag_". $tag => [
                            "gte" => 0.001
                        ]
                    ]
                ];
            }
            
            $query['filter'][] = [
                'bool' => [
                    'should' => [
                        $minProbClass
                    ]
                ]
            ];
        }
        
        include_once __DIR__ . '/LocationUtil.php';
        
        $childLocationIds = null;
        
        if($locationIdList != null) {
            
            try {
                $childLocationIds = LocationUtil::getAllChildlLocationIds($locationIdList);

                $query['filter'][] = [ 'terms' => [
                        "locationId" => $childLocationIds
                    ]
                ];
            }
            catch(Exception $e) {
                //not important
            }
        }
        
        
        $aggs = [];
        
        $aggs["location_bulks"] =  [
            "terms" => [
                "field" => "locationId"
            ]
        ];
        
        $aggs["job_type_bulks"] =  [
            "terms" => [
                "field"=> "jobType"
            ]
        ];
        
        /*
        if($restrictedClasses != null && !empty($restrictedClasses)) {
            
            $filters = null;
            
            outfor: foreach ($restrictedClasses as $class) {
                foreach ($queries as $term => $score) {
                    if($term === $class) {
                        continue 2;
                    }
                }
                
                $filters[$class] = [
                    "term" => [
                        "jobTag.keyword" => $class
                    ]
                ];
            }
            
            if($filters != null) {
                $aggs["related_class_bulks"] = [
                    "global" => new stdClass(),
                    "aggs" => [
                        "glob_filters" => [
                            "filter" => [
                                'range' => [
                                    "locationCode" => [
                                        "gte" => $locationProperties->lcMin,
                                        "lte" => $locationProperties->lcMax
                                    ]
                                ]
                            ],
                            "aggs" => [
                                "job_class_filters" => [
                                    "filters" => [
                                        "filters" => $filters
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];
            }
        }
         * 
         */
        
        $dateFilter = 0;
        if($lastSearchDate != null) {
            $dateFilter = $lastSearchDate;
        }
        else {
            $dateFilter = current_millis() - (1 * 30 * 24 * 60 * 60 * 1000); // 1 month ago
        }
        
        $query['filter'][] = [ 'range' => [
                'date' => [
                    "gte" => $dateFilter
                ]
            ]
        ];
        
        if($companyId != null) {
            $query['filter'][] = [ 'term' => [
                    'companyId' => $companyId
                ]
            ];
        }
        
        if ($queries != null) {
            
            $shouldQuery = [];
            
            if($maxProb > 0.8) {
                $shouldQuery[] = [
                    "match_all" => new stdClass()
                ];
            }
            
            foreach ($queries as $q) {
                $shouldQuery[] = [
                    "multi_match" => [
                        "query" => $q,
                        "fields" => ['title', 'body', 'jobTag']
                    ]
                ];
            }
            
            $query['must'] = [
                'bool' => [
                    'should' => $shouldQuery
                ]
            ];
        }
        
        $functions = [];
        
        $dateWeight = 1;
        
        if ($sortType == "date") {
            
            $scale = "7d";
            $dateWeight = 4;
            
            if($lastSearchDate != null) {
                $timeMiliesUntilNow = current_millis() - $lastSearchDate;
                $dayUntilNow = intval($timeMiliesUntilNow / (24*60*60*1000));
                
                if($dayUntilNow > 0) {
                    $scale = $dayUntilNow . "d";
                }
            }
        }
        else {
            $scale = "10d";
        }
        
        $functions[] = [
            "linear" => [
                "date" => [
                    "scale" => $scale
                ]
            ],
            "weight" => $dateWeight
        ];
        
        if($restrictedClasses != null) {
            
            foreach ($restrictedClasses as $class) {
                
                $functions[] = [
                    "field_value_factor" => [
                        "field" => "assignedJobTag_".$class["key"],
                        "factor" => $class["prob"],
                        "missing" => 0
                    ]
                ];
            }
        }
        
        $functionScore = [
            "query" => [
                'bool' => $query
            ],
            "functions" => $functions,
            "score_mode" => "sum",
            "boost_mode" => "sum"
        ];
        
        $body["query"] = [
            "function_score" => $functionScore
        ];
        
	$body["aggs"] = $aggs;
        
        $body["highlight"] = [
            "pre_tags" => ['start_highlight'],
            "post_tags" => ['end_highlight'],
            "fields" => [
                "body" => new stdClass()
            ]
        ];

        $from = $page * $pageSize;

        $elasticsearchQuery = [
            'index' => 'job',
            'type' => 'job',
            'from' => $from,
            'size' => $pageSize,
            'body' => $body
        ];
        
        //echo json_encode($body) ." *** ";
        
        $res = ElasticSearchHandler::$client->search($elasticsearchQuery);
        
        if($childLocationIds != null) {
            $bulks = array();
            foreach ($res['aggregations']['location_bulks']["buckets"] as $l) {
                if(in_array($l['key'], $childLocationIds) && !in_array($l['key'], $locationIdList) ) {
                    $bulks[] = $l;
                }
            }
            
            $res["aggregations"]["location_bulks"]["buckets"] = $bulks;
        }
        
        $res = $this->prepareJobTypesAggregation($res);
        //$res = $this->prepareRelevantClassAggregations($res, $restrictedClasses);
        
	return $res;
    }

    private function prepareJobTypesAggregation($res) {
        
        $buckets = array();
        foreach ($res["aggregations"]["job_type_bulks"]["buckets"] as $bucket) {
            $item["type"] = $bucket["key"];
            $item["count"] = $bucket["doc_count"];
            
            $buckets[] = $item;
        }
        
        $res["aggregations"]["job_type_bulks"] = $buckets;
                
        return $res;
    }
    
    private function prepareRelevantClassAggregations($res, $restrictedClass) {
        if(array_key_exists("related_class_bulks", $res["aggregations"])) {
            $relatedClassBulks = $res["aggregations"]["related_class_bulks"];
            $items = $relatedClassBulks["glob_filters"]["job_class_filters"]["buckets"];
            
            $relevantClasses = [];
            
            foreach ($restrictedClass as $class) {
                
                if(!array_key_exists($class, $items)) {
                    continue;
                }
                
                $relevantClasses[] = [
                    "key" => $class,
                    "count" => $items[$class]["doc_count"]
                ];
            }
            
            //$res["aggregations"]["related_class_bulks"] = $relevantClasses;
        }
        
        return $res;
    }
    
    /*
    public function spellCorrect($query) {
        
        $body["query"] = [
            "bool" => [
                "must" => [
                    [
                        "fuzzy" => [
                            "name.raw" => $query
                        ]
                    ]
                ]
            ]
        ];
        
        //echo json_encode($body) . " *** ";
        
        $elasticsearchQuery = [
            'index' => 'tag',
            'body' => $body,
            'from' => 0,
            'size' => 1
        ];
        
        $res = ElasticSearchHandler::$client->search($elasticsearchQuery);
        
        foreach ($res["hits"]["hits"] as $item) {
            $tag = $item["_source"]["name"];
            if($tag == $query) {
                return null;
            }
            
            return $tag;
        }
    }
    */
    
    public function getOneJobById($jobId) {
        
        $elasticsearchQuery = [
            'index' => 'job',
            'type' => 'job',
            'id' => $jobId
        ];
        
        return ElasticSearchHandler::$client->get($elasticsearchQuery);
    }

    public function getJobsByIds($jobIds) {
        
        $body["query"] = [
                "constant_score" => [
                    "filter" => [
                        "terms" => [
                            'id' => $jobIds
                        ]
                    ]
                ]
            ];
        
        $params = [
            'index' => 'job',
            'type' => 'job',
            'from' => 0,
            'size' => 40,
            'body' => $body
        ];

        return ElasticSearchHandler::$client->search($params);
    }

    public function getSimilarityJob($jobId) {
        
        include_once './SearchTools.php';
        include_once './LocationUtil.php';
        
        $originJob = $this->getOneJobById($jobId);
        
        $locationIdList = [];
        
        if(array_key_exists("locationId", $originJob['_source']) && !empty($originJob['_source']['locationId'])) {
            foreach ($originJob['_source']['locationId'] as $lc) {
                $locationIdList[] = $lc;
            }
        }
        
        if($locationIdList != null) {
            
            $allLocationIds = LocationUtil::getAllChildlLocationIds($locationIdList);
            
            $query['filter'][] = [ 'terms' => [
                    "locationId" => $allLocationIds
                ]
            ];
        }
        
        /*
        $restrictedClasses = [];
        $txt = "";
        foreach ($originJob['_source']['body'] as $body) {
            $txt .= $body . "\n";
        }
        include './RecommandSearch.php';
        $classifyResults = classify([$txt]);
        foreach ($classifyResults as $classifyResult) {
            foreach ($classifyResult as $cr) {
                if($cr["prob"] < 0.01) {
                    break;
                }
                $restrictedClasses[] = $cr["key"];
            }
        }
        if(!empty($restrictedClasses)) {
            $query['must'][] = [
                'terms' => [
                    'assignedJobTag' => $restrictedClasses
                ]
            ];
        }
         * 
         */
        
        $query['must'][] = [
            'range' => [
                'date' => [
                    'gte' => current_millis() - (1 * 30 * 24 * 60 * 60 * 1000) // one month ago
                ]
            ]
        ];
        
        $query['must'][] = [
            "more_like_this" => [
                    "fields" => ["title", "body", "jobTag", "jobType"],
                    "like" => [
                        [
                            "_index" => "job",
                            "_type" => "job",
                            "_id" => $originJob["_id"]
                        ]
                    ],
                    "min_term_freq" => 1,
                    "max_query_terms" => 30
            ]
        ];
        
        $body = [
            "query" => [
                "bool" => $query
            ]
        ];
        
        $elasticsearchQuery = [
            'index' => 'job',
            'type' => 'job',
            'from' => 0,
            'size' => 4,
            'body' => $body
        ];
        
        //echo json_encode($body);
        
        return ElasticSearchHandler::$client->search($elasticsearchQuery);
    }
    
    public function getPopularJobs() {
        $body = ["size" => 0, "aggs" => [
                "popular_jobs"=> [
                    "terms" => [
                        "field"=> "assignedJobTag",
                        "size" => 300
                    ]
                ]
            ]
        ];

        $elasticsearchQuery = [
            'index' => 'job',
            'type' => 'job',
            'body' => $body
        ];

        //print json_encode($elasticsearchQuery);

        return ElasticSearchHandler::$client->search($elasticsearchQuery);
    }
    
    public function getJobListByIds($jobList) {
        
        $body["query"]["constant_score"]["filter"]["terms"]["id"] = $jobList;

        $elasticsearchQuery = [
            'index' => 'job',
            'type' => 'job',
            'from' => 0,
            'size' => sizeof($jobList),
            'body' => $body
        ];
        
        return ElasticSearchHandler::$client->search($elasticsearchQuery);
    }
    
    public function countAllNewJobsLastWeek() {
        $query['filter'][] = [ 'range' => [
                'date' => ["gte" => (self::getInstance()->current_millis() - (7 * 24 * 60 * 60 * 1000) ) ]
            ]
        ];

        $body['query']['bool'] = $query;

        $elasticsearchQuery = [
            'index' => 'job',
            'type' => 'job',
            'body' => $body
        ];
        
        return ElasticSearchHandler::$client->count($elasticsearchQuery);
    }
            
    function current_millis() {
        list($usec, $sec) = explode(" ", microtime());
        return round(((float)$usec + (float)$sec) * 1000);
    }
}
