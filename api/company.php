<?php

require __DIR__ . '/../vendor/autoload.php';

if(isset($_GET["newComment"])) {
    
    $deviceUuid = $_GET['deviceUuid'];
    
    $entityBody = json_decode(file_get_contents('php://input'));
    $comment = $entityBody->comment;
    $stars = $entityBody->stars;
    $username = $entityBody->username;
    $companyId = $entityBody->company->id;
    
    $params = [
        'index' => 'comment',
        'type' => 'comment',
        'body' => [
            'deviceUuid' => $deviceUuid,
            'companyId' => $companyId,
            'comment' => $comment,
            'stars' => $stars,
            'username' => $username,
            'isEnabled' => false,
            'date' => current_millis()
        ]
    ];
    
    include_once './ElasticSearchHandler.php';
    $res = ElasticSearchHandler::getInstance()->getElasticClient()->index($params);
    
    unset($res['_index']);
    unset($res['_type']);
    unset($res['_shards']);
    unset($res['_version']);
    
    print json_encode($res);
}
elseif(isset ($_GET["getComments"])) {

    if(isset($_GET["page"])) {
        $page = $_GET["page"];
    }
    else {
        $page = 0;
    }
    
    $companyId = $_GET['companyId'];
    
    $elasticClient = Elasticsearch\ClientBuilder::create()->build();
    
    $params = [
        'index' => 'comment',
        'type' => 'comment',
        'from' => $page * 10,
        'size' => 10,
        'body' => [
            'query' => [
                'bool' => [
                    'must' => [ 
                            [
                                'term' => [
                                    'isEnabled' => true
                                ]
                            ],
                            [
                                'term' => [
                                    'companyId' => $companyId
                                ]
                            ]
                        ] 
                    ]
                ],
            'sort' => [
                [
                    'date' => [
                        'order'=> 'desc'
                    ]
                ]
            ]
        ]   
    ];
    
    $elasticClient = Elasticsearch\ClientBuilder::create()->build();
    
    $res = $elasticClient->search($params);

    print(json_encode($res['hits']['hits']));
    exit();
}
else if(isset ($_GET['companyId'])) {
    
    $companyId = $_GET['companyId'];
    
    $params = [
        'index' => 'company',
        'type' => 'company',
        'id' => $companyId
    ];
    
    $elasticClient = Elasticsearch\ClientBuilder::create()->build();
    
    $res = $elasticClient->get($params)["_source"];
    
    if(array_key_exists("photo", $res)) {
        for ($i = 0; $i< sizeof($res["photo"]); $i++) {
            $res["photo"][$i]["url"] = $_SERVER["SERVER_NAME"] 
                    . "/api/photo.php?id=". $res["photo"][$i]["id"];
        }
    }
    
    if(array_key_exists("sourceCrawle", $res)) {
        unset($res["sourceCrawle"]);
    }
    
    print(json_encode($res));
}

function current_millis() {
    list($usec, $sec) = explode(" ", microtime());
    return round(((float)$usec + (float)$sec) * 1000);
}
