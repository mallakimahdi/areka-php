<?php

require '../vendor/autoload.php';
include_once './DateUtil.php';

if(isset ($_GET["interactWithJob"])) {
    
    $action = $_GET["action"];
    
    $jobId = $_GET["jobId"];
    $searchQuery = $_GET["searchQuery"];
    $deviceUuid = $_GET["deviceUuid"];
    $date = current_millis();
    
    if($action == "favoriteClick" || $action == "shareClick" || $action == "sourceClick" 
            || $action == "phoneClick" || $action == "emailClick" 
            || $action == "companyClick" || $action == "viewJob") {
        
        $params = [
            'index' => 'statistic',
            'type' => 'jobInteraction',
            'body' => [
                'jobId' => $jobId,
                'searchQuery' => $searchQuery,
                'deviceUuid' => $deviceUuid,
                'date' => $date,
                'action' => $action
            ]
        ];
        
        include_once './ElasticSearchHandler.php';
        ElasticSearchHandler::getInstance()->getElasticClient()->index($params);
        
        $status = "success";
    }
    else {
        $status = "failed";
    }
    
    echo json_encode([
        "status" => $status
    ]);
}