<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/ElasticSearchHandler.php';

function saveSearchHistory($queries, $locationIdList, $jobType, $deviceUuid) {
    
    $searchHistory = [
        "query" => $queries,
        "locationId" => $locationIdList,
        "jobType" => $jobType,
        "deviceUuid" => $deviceUuid,
        "date" => current_millis()
    ];
    
    $query = [
        'index' => 'search_history',
        'type' => 'search_history',
        'body' => $searchHistory
    ];
    
    $res = ElasticSearchHandler::getInstance()->getElasticClient()->index($query);
    
    return $res["_id"];
}

function getSearchHistory($searchId) {
    
    $query = [
        'index' => 'search_history',
        'type' => 'search_history',
        'id' => $searchId
    ];
    
    $res = ElasticSearchHandler::getInstance()->getElasticClient()->get($query);
    return $res['_source'];
}

function saveTelegramSubscribtion($chatId, $searchId) {
    $query = [
        'index' => 'telegram_subscribtion',
        'type' => 'telegram_subscribtion',
        'id' => $chatId
    ];
    
    $telegramSub = null;
    try {
        $telegramSub = ElasticSearchHandler::getInstance()->getElasticClient()->get($query);
    }
    catch(Exception $e) { }
    
    if($telegramSub != null) {
        
        $previusSearchIds = $telegramSub["_source"]['searchId'];
        
        if(!in_array($searchId, $previusSearchIds)) {
            
            echo json_encode($previusSearchIds);
            
            $previusSearchIds[] = $searchId;
            
            $q = [
                'index' => 'telegram_subscribtion',
                'type' => 'telegram_subscribtion',
                'id' => $chatId,
                'body' => [
                    'doc' => [
                        'searchId' => $previusSearchIds
                    ]
                ]
            ];
            
            ElasticSearchHandler::getInstance()->getElasticClient()->update($q);
        }
    }
    else {
        $telegramSub = [
            'index' => 'telegram_subscribtion',
            'type' => 'telegram_subscribtion',
            'id' => $chatId,
            'body' => [
                'searchId' => [ $searchId ],
                'active' => true
            ]
        ];
        
        ElasticSearchHandler::getInstance()->getElasticClient()->index($telegramSub);
    }
}

function activeOrDeactiveTelegramUser($chatId, $activeOrDeactive) {
    $elasticClient = ElasticSearchHandler::getInstance()->getElasticClient();
    
    $params = [
        'index' => 'telegram_subscribtion',
        'type' => 'telegram_subscribtion',
        'id' => $chatId,
        'body' => [
            'doc' => [
                'active' => $activeOrDeactive
            ]
        ]
    ];

    $elasticClient->update($params);
}

function updateTelegramSubscribtion($id, $telegramSubscription) {
    $elasticClient = ElasticSearchHandler::getInstance()->getElasticClient();
    
    $params = [
        'index' => 'telegram_subscribtion',
        'type' => 'telegram_subscribtion',
        'id' => $id,
        'body' => $telegramSubscription
    ];

    $elasticClient->index($params);
}