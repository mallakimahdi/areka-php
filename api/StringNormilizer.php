<?php

function normalize($text) {
    
    $forms = "أإٱﭐﭑﴼﴽﺂﺃﺄﺇﺈﺍﺎ/ا*آﺁ/آ*بﺏﺐﺑﺒ/ب*ﭖﭗﭘﭙ/پ*تﺕﺖﺗﺘ/ت*ثﺙﺚﺛﺜ/ث*جﺝﺞﺟﺠ/ج*چﭺﭻﭼﭽ/چ*حﺡﺢﺣﺤ/ح*خﺥﺦﺧﺨ/خ*دﺩﺪ/د*ذﺫﺬ/ذ*رﺭﺮ/ر*زﺯﺰ/ز*ژﮊﮋ/ژ*سﺱﺲﺳﺴ/س*شﺵﺶﺷﺸ/ش*صﺹﺺﺻﺼ/ص*ضﺽﺾﺿﻀ/ض*طﻁﻂﻃﻄ/ط*ظﻅﻆﻇﻈ/ظ*ﻉﻊﻋﻌ/ع*غﻍﻎﻏﻐ/غ*فﻑﻒﻓﻔ/ف*قﻕﻖﻗﻘ/ق*كکﮎﮏﮐﮑﻙﻚﻛﻜ/ک*گﮒﮓﮔﮕ/گ*لﻝﻞﻟﻠ/ل*مﻡﻢﻣﻤ/م*نﻥﻦﻧﻨ/ن*وﻭﻮ/و*ؤﺅﺆ/ؤ*ۀةهﮤﮥﺓﺔﻩﻪﻫﻬ/ه*ىيیﯨﯼﯩﯽﯾﯿﻯﻰﻱﻲﻳﻴ/ی*ئﺉﺊﺋﺌ/ئ*ﻵﻶﻷﻸﻹﻺﻻﻼ/لا";
    
    $normalForms = [];
    
    foreach (explode("*", $forms) as $form) {
        $f = explode("/", $form);
        
        foreach (preg_split('//u', $f[0], -1, PREG_SPLIT_NO_EMPTY) as $char) {
            $normalForms[$char] = $f[1];
        }
    }
    
    $outputText = "";
    
    foreach (preg_split('//u', $text, -1, PREG_SPLIT_NO_EMPTY) as $char) {
        if(array_key_exists($char, $normalForms)) {
            $outputText .= $normalForms[$char];
        }
        else {
            $outputText .= $char;
        }
    }
    
    $outputText = strtolower($outputText);
    
    return $outputText;
}
