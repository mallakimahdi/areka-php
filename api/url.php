<?php

if(isset($_GET["url"])) {
    
    $url = $_GET["url"];
    
    $path = parse_url($url, PHP_URL_PATH);
    $jobId = lastIndexOf("/");
    $date = current_millis();
    $deviceUuid = $_GET["deviceUuid"];
    $resultIndex = $_GET["resultIndex"];
    $searchId = $_GET["searchId"];
    
    saveVisitJob($deviceUuid, $searchId, $jobId, $date, $resultIndex);
    
    header("Location: ". $url);
    die();
}